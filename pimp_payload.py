#!/usr/bin/python
# -*- coding: utf8 -*-
__author__ = "stk <lucam.ko@gmail.com>"
__version__ = "$Revision: 0.1 $"
__date__ = "$Date: 2013/05/20 $"
__copyright__ = "Copyright (c) 2012-2013 Luca Mella"
__license__ = "CC BY-NC-SA"

"""
SYNOPSIS

	Pimp payload is a python based preprocessor which enable to personalize 
	and compile a shellcode script written in Assembly language (NASM compatible) 

DESCRIPTION

	Pimp payload search for proper tag in the shellcode source code and then 
	evaluate the tagged code as python source code, then it replace the tagged 
	code with the evaluation of that python chunk.

	User can specify variables values to be used during python code evaluation.
	This way is possible to customize shellcode script enanching the shellcode reuse.

EXAMPLES

	Shellcode scripters should add python chunk by surrounding it with $ character

	Eg.
		xor bl,$hex(int(KEY))$	

	Then by launching pimp shellcode with KEY=127 parameter the obtained result 
	is 
		xor bl,0x7f

	NOTE: parameters are interpreted as STRINGS

	Full example

		./pimp_payload.py  linux64_bash_command.s COMMAND='echo "generated"' KEY=11

	Pimp payload use the folder "payloads" as payload database. 
	
	/pimp_payload.py -h
		usage: pimp_payload.py [-h] [-l] [-w] [-r] [PAYLOAD] [PARAM [PARAM ...]]

		Pimp some payload

		positional arguments:
	  		PAYLOAD     The selected payload
	  		PARAM       Payload parameters

		optional arguments:
	  		-h, --help  show this help message and exit
	  		-l, --list  Print the payloads
	  		-w, --what  Print the payload comments and info
	  		-r, --raw   Output the shellcode in raw mode (default hex encoding


EXIT STATUS

	0

AUTHOR

    $__author__$

LICENSE

    This script is in the public domain, free from copyrights or restrictions.

VERSION

    $__version__$
"""



import argparse
import os
import tempfile
import subprocess
import re
import sys
import string

SHELLCODEFOLDER='payloads'+os.sep
FILTER=lambda f: f.endswith('.s')
PARAM_REGEX=re.compile("\$(.*?)\$")
COMMENTS_REGEX=re.compile("\;\#(.*)")
ASSEMBLE='/usr/bin/nasm|%s|-o|%s'

def list_payloads(folder,filter=FILTER):
	""" """
	outl=list()
	for (path,dirs,files) in os.walk(folder):
		for f in files:
			if filter(f):
				outl+=[(path,f)]
	return outl

def list_params(payloadfile):
	""" """
	with open(payloadfile) as pf:
		payload=pf.read()
		return PARAM_REGEX.findall(payload)
def get_comments(payloadfile):
	""" """
	with open(payloadfile) as pf:
		payload=pf.read()
		return '\n'.join(COMMENTS_REGEX.findall(payload))

def pimp_payload(payloadfile,paramsbindings):
	pimped=''
	with open(payloadfile,'r') as pf:
		payload=pf.read()
		for par in PARAM_REGEX.findall(payload):
			val=eval(par,paramsbindings)
			payload=payload.replace("$"+par+"$",val)
		shellc=tempfile.mkstemp()[1]
		
		with open(shellc,'w') as tmp:
			tmp.write(payload)
			p=subprocess.Popen(string.split(ASSEMBLE%(shellc,shellc+'.o'),'|'), 
			stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
		p.wait()
		if p.returncode==0:
			with open(shellc+'.o','r') as tmp:
				pimped=tmp.read()
			os.unlink(shellc+'.o')
			os.unlink(shellc)
			return pimped
		else:
			print >> sys.stderr, 'Something Wrong during assembling:\n%s' % p.stderr.read()
	return pimped
	
if __name__=='__main__':

	parser = argparse.ArgumentParser(description='Pimp some payload')
	parser.add_argument('-l','--list', dest='list', action='store_true',
					help='Print the payloads')
	parser.add_argument('-w','--what',dest='what', action='store_true',
					help='Print the payload comments and info')
	parser.add_argument('-r','--raw',dest='raw', action='store_true',
					help='Output the shellcode in raw mode (default hex encoding)')
	parser.add_argument('payload', metavar='PAYLOAD', action='store', nargs='?',
					default=None, help='The selected payload')
	parser.add_argument('param', metavar='PARAM', action='append', nargs='*',
					help='Payload parameters')

	args = parser.parse_args()

	if args.list is True:
		payloads=list_payloads(SHELLCODEFOLDER)
		print "Payloads:"
		print ""
		for pth,p in payloads:
			print p
		print ""
		print "Use --what <PAYLOADNAME> for payload parameter detail"
		exit(0)
	if args.what is True and args.payload is not None:
		comment=get_comments(SHELLCODEFOLDER+args.payload)
		formals=list_params(SHELLCODEFOLDER+args.payload)
		print "Payload %s " % args.payload
		print "+++"
		print comment
		print "+++"
		#print "FORMAL PARAMETERS:"
		#for f in formals:
		#	print "\t%s"%f
		#print ""
		print "Use <PAYLOADNAME>  [<FORMAL>=<VALUE>]* for parameters value specification"
		exit(0)
	if args.payload is not None :
		binding=dict()
		for p in args.param[0]:
			formal,value=p.split('=',1)
			binding[formal]=value
		pimped=pimp_payload(SHELLCODEFOLDER+args.payload,binding)
		if args.raw:
			print pimped 
		else:
			print pimped.encode('hex')