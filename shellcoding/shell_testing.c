/**
 * Shellcode tester
 * Daniele Bellavista
 * CeSeNA (Cesena Security & Network Application)
 */

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <execinfo.h>

#if __x86_64__ || __ppc64__
#define INFO() { \
	asm("push %rdi");\
	asm("push %rsi");\
	asm("push %rbp");\
	asm("push %rsp");\
	asm("push %rdx");\
	asm("push %rcx");\
	asm("push %rbx");\
	asm("push %rax");\
	asm("push $0x666666");\
	printf("rax: 0x%X\nrbx: 0x%X\nrcx: 0x%X\nrdx: 0x%X\nrsp: 0x%X\nrbp: 0x%X\nrsi: 0x%X\nrdi: 0x%X\n");\
}
#else
#define INFO() { \
	asm("push %edi");\
	asm("push %esi");\
	asm("push %ebp");\
	asm("push %esp");\
	asm("push %edx");\
	asm("push %ecx");\
	asm("push %ebx");\
	asm("push %eax");\
	asm("push $0x666666");\
	printf("eax: 0x%X\nebx: 0x%X\necx: 0x%X\nedx: 0x%X\nesp: 0x%X\nebp: 0x%X\nesi: 0x%X\nedi: 0x%X\n");\
}
#endif

static const int MAX_SIZE = 1024;

void handler(int signal);
void test_shellcode(void * shellcode);

void handler(int signal) {
	printf("\nException: ");
	switch(signal) {
	case SIGSEGV:
			printf("Segmentation fault!\n");
		break;
	case SIGILL:
			printf("Illegal Instruction!\n");
		break;
	case SIGFPE:
			printf("Floating point exception!\n");
		break;
	}
	printf("\nTrace: \n");

	void *array[100];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 100);
  backtrace_symbols_fd(array, size, 2);

  printf("\n");

	INFO()

	exit(signal + 10);
}

void test_shellcode(void * shellcode) {
	(*(void (*)(void)) shellcode)();
	INFO()
}

size_t read_shellcode(const char* filename, void** out_memory) {
	FILE *fp = fopen(filename, "r");
	if(fp == NULL) {
		perror("Opening file");
		exit(1);
	}

	fseek(fp, 0L, SEEK_END);
	long fileLength = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	if(fileLength > MAX_SIZE) {
		fprintf(stderr, "File too long (%ld)!", fileLength);
		return 0;
	}
	size_t tot_length = fileLength + 10;

	*out_memory = mmap(NULL, tot_length, PROT_EXEC | PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if(*out_memory == MAP_FAILED) {
		perror("Error mapping memory");
		exit(2);
	}

	size_t size = fread(*out_memory, sizeof(char), fileLength, fp);
	if(size != fileLength) {
		perror("Error reading from file");
	}
	fclose(fp);
	return size;
}

/**
 * Insert a ret code (C3) into the given position
 */
void insert_ret_code(char* shellcode, int position) {
	shellcode[position] = 0xc3;
}

void print_shellcode(unsigned char* shellcode, size_t size) {
	unsigned int i = 0;
	for(; i < size; i++) {
		printf("\\x%02x", shellcode[i]);
	}
}

int main(int argc, char** argv) {
	if(argc < 2) {
		printf("usage: %s <file>\n", argv[0]);
		return 1;
	}

	signal(SIGSEGV, handler);
	signal(SIGFPE, handler);
	signal(SIGILL, handler);

	void* shellcode;
	size_t len = read_shellcode(argv[1], &shellcode);
	printf("Shellcode length: %lu\n", len);
	insert_ret_code(shellcode, len);

	printf("Shellcode:\n");
	print_shellcode((unsigned char*) shellcode, len);
	printf("\n");

	printf("Executing shellcode...\n\n");
	test_shellcode(shellcode);
	printf("\nShellcode executed\n");

	munmap(shellcode, len);
	return 0;
}
